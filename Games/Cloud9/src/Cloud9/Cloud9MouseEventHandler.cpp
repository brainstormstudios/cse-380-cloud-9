#include "cloud9_VS\stdafx.h"
#include "cloud9\cloud9MouseEventHandler.h"
#include "mg\game\Game.h"
#include "mg\gsm\sprite\SpriteManager.h"
#include "mg\gsm\state\GameStateManager.h"
#include "mg\gui\Viewport.h"

void cloud9MouseEventHandler::handleMousePressEvent(int mouseX, int mouseY)
{
	Game *game = Game::getSingleton();
	if (game->getGSM()->isGameInProgress())
	{
		GameStateManager *gsm = game->getGSM();
		SpriteManager *spriteManager = gsm->getSpriteManager();
		Viewport *viewport = game->getGUI()->getViewport();
		
		// DETERMINE WHERE ON THE MAP WE HAVE CLICKED
		int worldCoordinateX = mouseX + viewport->getViewportX();
		int worldCoordinateY = gsm->getWorld()->getWorldHeight() - (mouseY + viewport->getViewportY());
		float x = ((float)worldCoordinateX) / 64.0f + 0.5f;
		float y = ((float)worldCoordinateY) / 64.0f + 0.5f;

		// Move cloud to that location
	
			Player *player = spriteManager->getPlayer();
			if (player->getBot()->getCurrentState() != L"DYING")
				player->moveCloud(x, y);
			else
				gsm->loadCurrentLevel();
	}
}

void cloud9MouseEventHandler::handleMouseMoveEvent(int mouseX, int mouseY)
{
	Game *game = Game::getSingleton();

	// DETERMINE WHAT THE PATHFINDING GRID CELL
	// IS THAT THE MOUSE CURRENTLY IS ABOVE
	if (game->getGSM()->isGameInProgress())
	{
		// IF YOU LIKE YOU COULD DO SOME MOUSE OVER DEBUGGING STUFF HERE
	}
}