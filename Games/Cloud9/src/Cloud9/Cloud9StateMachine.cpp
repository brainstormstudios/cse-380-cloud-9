#include "mg_VS\stdafx.h"
#include "cloud9StateMachine.h"
#include "mg\game\Game.h"
#include "mg\gsm\state\GameStateManager.h"

void cloud9StateMachine::update()
{
	Game *game = Game::getSingleton();
	GameStateManager *gsm = game->getGSM();
	SpriteManager *spriteManager = gsm->getSpriteManager();

	// IS THE GAME GOING ON?
	if (gsm->isGameInProgress())
	{
		
		list<Bot*>::iterator botIterator = spriteManager->getBotsIterator();
		list<Bot*>::iterator end = spriteManager->getEndOfBotsIterator();
		string door = "door";
		string key = "key";

		Bot *player = spriteManager->getPlayer()->getBot();
		float playerX = player->getPhysicalProperties()->GetPosition().x;
		float playerY = player->getPhysicalProperties()->GetPosition().y;
		float playerWidth = player->getSpriteType()->getTextureWidth() / 64;
		float playerHeight = player->getSpriteType()->getTextureHeight() / 64;

		while (botIterator != end) {
			Bot *bot = (*botIterator);
			// Is the player at the exit?
			if (bot->getType() == wstring(door.begin(), door.end())) {
				float exitX = bot->getPhysicalProperties()->GetPosition().x;
				float exitY = bot->getPhysicalProperties()->GetPosition().y;
				float exitWidth = (float)bot->getSpriteType()->getTextureWidth() / 64.0;
				float exitHeight = (float)bot->getSpriteType()->getTextureHeight() / 64.0;
				if (playerX + playerWidth/2 > exitX - exitWidth/2 && playerX - playerWidth/2 < exitX + exitWidth/2 &&
					playerY + playerHeight/2 > exitY - exitHeight/2 && playerY - playerHeight/2 < exitY + exitHeight/2)
					if(spriteManager->getPlayer()->getKeysLeft() == 0)
					gsm->goToLoadLevel();

			}
			//Is the player at a key?
			if (bot->getType() == wstring(key.begin(), key.end())) {
				float keyX = bot->getPhysicalProperties()->GetPosition().x;
				float keyY = bot->getPhysicalProperties()->GetPosition().y;
				float keyWidth = (float)bot->getSpriteType()->getTextureWidth() / 64.0;
				float keyHeight = (float)bot->getSpriteType()->getTextureHeight() / 64.0;
				if (playerX + playerWidth/2 > keyX - keyWidth/2 && playerX - playerWidth/2 < keyX + keyWidth/2 &&
					playerY + playerHeight/2 > keyY - keyHeight/2 && playerY - playerHeight/2 < keyY + keyHeight/2) {
					bot->markForRemoval();
					spriteManager->getPlayer()->setKeysLeft(spriteManager->getPlayer()->getKeysLeft() - 1);
				}
			}
			botIterator++;
		}

		if (player->getPhysicalProperties()->GetPosition().y <= 0 && player->getCurrentState() != L"DYING") {
			player->setCurrentState(L"DYING");
			player->getPhysicalProperties()->SetLinearVelocity(b2Vec2(0.0f, 15.0f));
		}

		/*if (spriteManager->getNumberOfBots() == 0)
		{
			gsm->goToLoadLevel();
		}*/
	}
	else if (gsm->isPreGame())
	{
		gsm->goToLoadLevel();
	}
	else if (gsm->isGameLevelLoading())
	{
		// NOW WE NEED TO TRANSITION TO THE NEXT LEVEL
		// BUT NOTE WE'RE HARD CODING THIS IN THAT WE
		// KNOW HOW MANY LEVELS THERE ARE, WHICH ISN'T IDEAL
		if ((gsm->getCurrentLevelIndex() < 7) ||
			(gsm->getCurrentLevelIndex() == NO_LEVEL_LOADED))
		{
			// WE'LL GO ONTO LEVEL INDEX 1
			gsm->loadNextLevel();
			gsm->goToGame();
			game->getGraphics()->setDebugTextShouldBeRendered(true);

		}
		else
		{
			// WE ONLY HAVE 2 LEVELS SO WHEN WE FINISH
			// THE SECOND ONE JUST GO BACK TO THE MAIN MENU
			gsm->goToMainMenu();
			game->getGraphics()->setDebugTextShouldBeRendered(false);
		}
	}
}