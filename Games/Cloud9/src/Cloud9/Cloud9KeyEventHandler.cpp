/*
	Author: Richard McKenna
			Stony Brook University
			Computer Science Department

	cloud9KeyEventHandler.cpp

	See cloud9KeyEventHandler.h for a class description.
*/

#include "cloud9_VS\stdafx.h"
#include "cloud9\cloud9App.h"
#include "cloud9\cloud9KeyEventHandler.h"
#include "mg\clock\GameClock.h"
#include "mg\game\Game.h"
#include "mg\game\WStringTable.h"
#include "mg\graphics\GameGraphics.h"
#include "mg\gsm\physics\Physics.h"
#include "mg\gsm\physics\PhysicalProperties.h"
#include "mg\gsm\sprite\AnimatedSprite.h"
#include "mg\Box2d\GroundedContactListner.h"
#include "mg\gsm\state\GameState.h"
#include "mg\gsm\state\GameStateManager.h"
#include "mg\gui\Cursor.h"
#include "mg\gui\GameGUI.h"
#include "mg\input\GameInput.h"

/*
	handleKeyEvent - this method handles all keyboard interactions. Note that every frame this method
	gets called and it can respond to key interactions in any custom way. Ask the GameInput class for
	key states since the last frame, which can allow us to respond to key presses, including when keys
	are held down for multiple frames.
*/
void cloud9KeyEventHandler::handleKeyEvents()
{
	Game *game = Game::getSingleton();

	// WE CAN QUERY INPUT TO SEE WHAT WAS PRESSED
	GameInput *input = game->getInput();

	// LET'S GET THE PLAYER'S PHYSICAL PROPERTIES, IN CASE WE WANT TO CHANGE THEM
	GameStateManager *gsm = game->getGSM();
	SpriteManager    *spriteManager = gsm->getSpriteManager();
	Player *player = spriteManager->getPlayer();
//	PhysicalProperties *pp = player->getPhysicalProperties();
	Viewport *viewport = game->getGUI()->getViewport();

	boolean running = false;
	boolean isGrounded;
	// IF THE GAME IS IN PROGRESS
	if (gsm->isGameInProgress())
	{
		if (input->isKeyDownForFirstTime(P_KEY))
		{
			//gsm->getPhysics()->togglePhysics();
		}
		if (input->isKeyDownForFirstTime(T_KEY))
		{
			//gsm->getPhysics()->activateForSingleUpdate();
		}
		/*if (input->isKeyDownForFirstTime(D_KEY))
		{
			//viewport->toggleDebugView();
			game->getGraphics()->toggleDebugTextShouldBeRendered();
		}*/

		bool viewportMoved = false;
		float viewportVx = 0.0f;
		float viewportVy = 0.0f;
		if (player->getBot()->getCurrentState() != L"DYING") {
			if (input->isKeyDownForFirstTime(W_KEY))
				{
				b2Body *pp = player->getBot()->getPhysicalProperties();
				if (player->getSensor()->numFootContacts > 0 && pp->GetLinearVelocity().y <= 0.01 && pp->GetLinearVelocity().y >= -0.01) {
					pp->ApplyLinearImpulse(b2Vec2(0.0f, 13.0f), pp->GetLocalCenter(), true);
					if (pp->GetLinearVelocity().x < 0)
						player->getBot()->setCurrentState(L"JUMP_LEFT");
					else
						player->getBot()->setCurrentState(L"JUMP_RIGHT");
				}


					/*viewportVy -= MAX_VIEWPORT_AXIS_VELOCITY;
					viewportMoved = true;*/
				}
				if (input->isKeyDown(DOWN_KEY))
				{
					/*viewportVy += MAX_VIEWPORT_AXIS_VELOCITY;
					viewportMoved = true;*/
				}

			if (input->isKeyDown(A_KEY))
				{
					/*viewportVx -= MAX_VIEWPORT_AXIS_VELOCITY;
					viewportMoved = true;*/
					b2Body *pp = player->getBot()->getPhysicalProperties();
					if(pp->GetLinearVelocity().x > -4.0f)
					pp->ApplyLinearImpulse(b2Vec2(-3.0f, 0.0f),pp->GetLocalCenter(),true);
					running = true;
					if (!pp->GetLinearVelocity().y > 0)
						//player->getBot()->setBotState(MOVING);
						player->getBot()->setCurrentState(L"WALKING_LEFT");
					if (pp->GetLinearVelocity().y < 0)
						player->getBot()->setCurrentState(L"IDLE_LEFT");
			
				}
				if (input->isKeyDown(D_KEY))
				{
					/*viewportVx += MAX_VIEWPORT_AXIS_VELOCITY;
					viewportMoved = true;*/
					b2Body *pp = player->getBot()->getPhysicalProperties();
					if (pp->GetLinearVelocity().x < 4.0f)
					pp->ApplyLinearImpulse(b2Vec2(3.0f, 0.0f), pp->GetLocalCenter(), true);
					running = true;
					if (!pp->GetLinearVelocity().y > 0)
						//player->getBot()->setBotState(MOVING);
						player->getBot()->setCurrentState(L"WALKING_RIGHT");
					if (pp->GetLinearVelocity().y < 0)
						player->getBot()->setCurrentState(L"IDLE_RIGHT");
				}
				if (viewportMoved)
					viewport->moveViewport((int)floor(viewportVx+0.5f), (int)floor(viewportVy+0.5f), game->getGSM()->getWorld()->getWorldWidth(), game->getGSM()->getWorld()->getWorldHeight());
				if (!running) {
					b2Body *pp = player->getBot()->getPhysicalProperties();
					float slowDownImpulse = 0 - pp->GetLinearVelocity().x/2;
					pp->ApplyLinearImpulse(b2Vec2(slowDownImpulse, 0.0f), pp->GetLocalCenter(), true);

					player->getBot()->setBotState(NONE);
					if (player->getSensor()->numFootContacts > 0 && pp->GetLinearVelocity().y <= 0.01 && pp->GetLinearVelocity().y >= -0.01) {
						if (slowDownImpulse > 0)
							player->getBot()->setCurrentState(L"IDLE_LEFT");
						else
							player->getBot()->setCurrentState(L"IDLE_RIGHT");
					}
				}
		}
		if (input->isKeyDownForFirstTime(ONE_KEY))
		{
			gsm->goToLoadLevel();
		}

		if (input->isKeyDownForFirstTime(R_KEY))
		{
			gsm->loadCurrentLevel();
		}
	}
	/*
	// 0X43 is HEX FOR THE 'C' VIRTUAL KEY
	// THIS CHANGES THE CURSOR IMAGE
	if ((input->isKeyDownForFirstTime(C_KEY))
		&& input->isKeyDown(VK_SHIFT))
	{
		Cursor *cursor = game->getGUI()->getCursor();
		unsigned int id = cursor->getActiveCursorID();
		id++;
		if (id == cursor->getNumCursorIDs())
			id = 0;		
		cursor->setActiveCursorID(id);
	}

	// LET'S MESS WITH THE TARGET FRAME RATE IF THE USER PRESSES THE HOME OR END KEYS
	GameClock *clock = game->getClock();
	int fps = clock->getTargetFPS();

	// THIS SPEEDS UP OUR GAME LOOP AND THUS THE GAME, NOTE THAT WE COULD ALTERNATIVELY SCALE
	// DOWN THE GAME LOGIC (LIKE ALL VELOCITIES) AS WE SPEED UP THE GAME. THAT COULD PROVIDE
	// A BETTER PLAYER EXPERIENCE
	if (input->isKeyDown(VK_HOME) && (fps < MAX_FPS))
		clock->setTargetFPS(fps + FPS_INC);

	// THIS SLOWS DOWN OUR GAME LOOP, BUT WILL NOT GO BELOW 5 FRAMES PER SECOND
	else if (input->isKeyDown(VK_END) && (fps > MIN_FPS))
		clock->setTargetFPS(fps - FPS_INC);*/
}