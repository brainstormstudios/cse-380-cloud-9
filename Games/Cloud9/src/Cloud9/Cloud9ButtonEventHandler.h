/*	
	Author: Richard McKenna
			Stony Brook University
			Computer Science Department

	cloud9ButtonEventHandler.h

	This is a custom button event handler. Each game can choose
	to use different buttons and respond differently to when
	buttons are pressed. This class will handle button presses for
	the cloud9Game following the ButtonEventHandler pattern.
*/

#pragma once
#include "cloud9_VS\stdafx.h"
#include "mg\input\ButtonEventHandler.h"

class Game;

const wstring	GO_TO_MM_COMMAND = L"Go To Main Menu";
const wstring	START_COMMAND = L"Start";
const wstring	EXIT_COMMAND = L"Exit";
const wstring	QUIT_COMMAND = L"Quit";

class cloud9ButtonEventHandler: public ButtonEventHandler
{
public:
	// AS-IS, THIS CLASS HAS NO DATA, SO THERE IS NOTHING
	// TO INITIALIZE OR DESTROY
	cloud9ButtonEventHandler()	{}
	~cloud9ButtonEventHandler()	{}

	// DEFINED INSIDE cloud9ButtonEventHandler.cpp
	void handleButtonEvents(wstring command);
};