#pragma once

#include "mg\game\Game.h"
#include "mg\input\MouseEventHandler.h"

class cloud9MouseEventHandler : public MouseEventHandler
{
public:
	cloud9MouseEventHandler() {}
	~cloud9MouseEventHandler() {}
	void handleMousePressEvent(int mouseX, int mouseY);
	void handleMouseMoveEvent(int mouseX, int mouseY);
};