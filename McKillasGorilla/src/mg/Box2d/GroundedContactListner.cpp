#include "mg_VS/stdafx.h"
#include "GroundedContactListner.h"
GroundedContactListner::GroundedContactListner()
{
}


GroundedContactListner::~GroundedContactListner()
{
}


void GroundedContactListner::BeginContact(b2Contact *contact)
{
	//check if fixture A was the foot sensor
	void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	if ((int)fixtureUserData == 3)
		numFootContacts++;
	//check if fixture B was the foot sensor
	fixtureUserData = contact->GetFixtureB()->GetUserData();
	if ((int)fixtureUserData == 3)
		numFootContacts++;
}

void GroundedContactListner::EndContact(b2Contact *contact)
{
	//check if fixture A was the foot sensor
	void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	if ((int)fixtureUserData == 3)
		numFootContacts--;
	//check if fixture B was the foot sensor
	fixtureUserData = contact->GetFixtureB()->GetUserData();
	if ((int)fixtureUserData == 3)
		numFootContacts--;
}
