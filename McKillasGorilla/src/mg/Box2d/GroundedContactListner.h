#pragma once
#include "mg_VS\stdafx.h"
#include <Box2D\Box2D.h>
/*This class is a contact listner that keeps track of the # of objects that are under the player
If the # of Contacts > 0 then the player can jump*/
class GroundedContactListner :
	public b2ContactListener
{
public:
	int numFootContacts = 0;
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
	GroundedContactListner();
	~GroundedContactListner();
};

