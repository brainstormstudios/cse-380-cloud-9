#pragma once
class box2dPhysics
#include <Box2D/Box2D.h>
#include <stdio.h>
{
private:
	b2World* world;
public:
	box2dPhysics();
	~box2dPhysics();

	void createWorld();
	void deleteWorld();
	void loadWorldFromTiles();
	
	
	b2World* getWorld() { return world; };
	
};

