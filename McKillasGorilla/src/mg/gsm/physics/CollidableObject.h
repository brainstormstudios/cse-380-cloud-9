#pragma once
#include "mg_VS\stdafx.h"
#include "mg\gsm\physics\PhysicalProperties.h"
#include <Box2D\Box2D.h>

class CollidableObject
{
protected:
	bool currentlyCollidable;
	b2Body* pp;

public:
	CollidableObject()	{}
	virtual ~CollidableObject()	{}

	// INLINED METHODS
	bool				isCurrentlyCollidable()		{ return currentlyCollidable;	}
	b2Body* getPhysicalProperties()		{ return pp;					}

	void setCurrentlyCollidable(bool initCurrentlyCollidable)
	{	currentlyCollidable = initCurrentlyCollidable; }
};