#include "mg_VS/stdafx.h"
#include "Player.h"


Player::Player()
{
}


Player::~Player()
{
}

void Player::initFootSensor()
{
	b2PolygonShape polygonShape;
	b2FixtureDef myFixtureDef;

	polygonShape.SetAsBox(0.3, 0.3, b2Vec2(0, -1.5), 0);
	myFixtureDef.isSensor = true;
	myFixtureDef.shape = &polygonShape;
	myFixtureDef.density = 1;
	b2Fixture* footSensorFixture = playerBot->getPhysicalProperties()->CreateFixture(&myFixtureDef);
	footSensorFixture->SetUserData((void*)3);

	footSensor.numFootContacts = 0;
}
void Player::moveCloud(float x, float y)
{
	if (numClouds > 0) {
		cloudBot->getPhysicalProperties()->SetTransform(b2Vec2(x, y), 0);
		numClouds--;
	}
}
