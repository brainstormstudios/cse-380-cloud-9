/*
	Author: Richard McKenna
			Stony Brook University
			Computer Science Department

	AnimatedSprite.cpp

	See AnimatedSprite.h for a class description.
*/

#include "mg_VS\stdafx.h"
#include "mg\game\Game.h"
#include "mg\gsm\physics\PhysicalProperties.h"
#include "mg\gsm\sprite\AnimatedSprite.h"
#include "mg\gsm\sprite\AnimatedSpriteType.h"
#include "mg\gsm\state\GameStateManager.h"
#include <stdio.h>

/*
	AnimatedSprite - Default constructor, just sets everything to 0.
*/
AnimatedSprite::AnimatedSprite()  
{
	Game *game = Game::getSingleton();
	GameStateManager *gsm = game->getGSM();
	spriteType = 0;
	frameIndex = 0;
	animationCounter = 0;
	//pp.setVelocity(0.0f, 0.0f);
	//pp.setAccelerationX(0.0f);
	//pp.setAccelerationY(0.0f);
	//pp.setPosition(0.0f, 0.0f);
	markedForRemoval = false;
}

/*
	Destructor - Nothing to clean up. We don't want to clean up the
	sprite type because it is a shared variable.
*/
AnimatedSprite::~AnimatedSprite() 
{

}

/*
	changeFrame - This method allows for the changing of an image in an 
	animation sequence for a given animation state.
*/
void AnimatedSprite::changeFrame()
{
	// RESET THE COUNTER
	animationCounter = 0;

	// REMEMBER, WE COUNT BY 2s
	frameIndex += 2;

	// GO BACK TO FIRST INDEX IF NECESSARY
	if (frameIndex == spriteType->getSequenceSize(currentState))
		frameIndex = 0;
}

/*
	getCurrentImageID - This method does the work of retrieving
	the image id that corresponds to this sprite's current state
	and frame index.
*/
unsigned int AnimatedSprite::getCurrentImageID()
{
	return spriteType->getAnimationFrameID(currentState, frameIndex);
}

/*
	setCurrentState - This method sets this sprite to the newState
	state and starts its animtion sequence from the beginning.
*/
void AnimatedSprite::setCurrentState(wstring newState) 
{
	string cs(currentState.begin(), currentState.end());
	string ns(newState.begin(), newState.end());
	if (strcmp(cs.c_str(), ns.c_str()) != 0)
	{
		// SET THE ANIMATINO STATE
		currentState = newState;

		// AND RESET OUR COUNTERS
		animationCounter = 0;
		frameIndex = 0;
	}
}

/*
	updateSprite - To be called every frame of animation. This
    method advances the animation counter appropriately per
	the animation speed. It also updates the sprite location
	per the current velocity.
*/
void AnimatedSprite::updateSprite()
{
	unsigned int duration = spriteType->getDuration(currentState, frameIndex);
	animationCounter++;

	// WE ONLY CHANGE THE ANIMATION FRAME INDEX WHEN THE
	// ANIMATION COUNTER HAS REACHED THE DURATION
	if (animationCounter >= duration)
		changeFrame();

	// AND NOW CALCULATE THE RADIANS
	// FOR RENDERING THIS SPRITE
	rotationInRadians = 0;
}

void AnimatedSprite::initB2Body(float x, float y)
{
	GameStateManager *gsm = Game::getSingleton()->getGSM();
	b2World *world = gsm->getPhysics();
	
	//sets initial position in tile# : 1 tile = 1 meter
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(x, y);
	bodyDef.linearDamping = 0.0f;
	bodyDef.fixedRotation = true;
	b2Body* body = world->CreateBody(&bodyDef);
	pp = body;
	b2FixtureDef fixtureDef;
	

	//Convert Sprite dimesnsions to meters 64pixels = 1 meter
	float width;
	float height;
	width = this->getSpriteType()->getTextureWidth() / 64.0f;
	height = this->getSpriteType()->getTextureHeight() / 64.0f;
	b2PolygonShape shape;
	shape.SetAsBox(width / 2.0f, height /2.0f);
	
	fixtureDef.shape = &shape;
	// Set the box density to be non-zero, so it will be dynamic.
	fixtureDef.density = 1.0f;
	// Override the default friction.
	fixtureDef.friction = 0.0f;
	//No Bouncing Sprites!
	fixtureDef.restitution = 0.0f;
	// Add the shape to the body.
	pp->CreateFixture(&fixtureDef);
}
