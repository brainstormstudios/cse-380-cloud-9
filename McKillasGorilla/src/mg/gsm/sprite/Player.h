#pragma once
#include "mg\gsm\ai\Bot.h"
#include "mg\Box2d\GroundedContactListner.h"
class Player
{
private:
	GroundedContactListner	footSensor;
	Bot*						playerBot;
	Bot*						cloudBot;
	int							numClouds;
	int							keysLeft;
public:
	Player();
	~Player();
	void initFootSensor();
	void setBot(Bot *initBot)	{ playerBot = initBot;		}
	Bot* getBot()				{ return playerBot;			}
	Bot* getCloud()				{ return cloudBot;			}
	void initCloud(Bot *initBot) { cloudBot = initBot;		}
	void setNumClouds(int initNumClouds) { numClouds = initNumClouds; }
	int getNumClouds()					{ return numClouds; }
	int getKeysLeft() { return keysLeft; }
	void setKeysLeft(int initNumKeys) { keysLeft = initNumKeys; }
	GroundedContactListner*		getSensor() { return &footSensor; }
	void moveCloud(float x, float y);

};

